#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>

#define BODY_SIZE 100

void dumphex(const void* data, size_t size) {
  char ascii[17];
  size_t i, j;
  ascii[16] = '\0';
  for (i = 0; i < size; ++i) {
    printf("%02X ", ((unsigned char*)data)[i]);
    if (((unsigned char*)data)[i] >= ' ' && ((unsigned char*)data)[i] <= '~') {
      ascii[i % 16] = ((unsigned char*)data)[i];
    } else {
      ascii[i % 16] = '.';
    }
    if ((i+1) % 8 == 0 || i+1 == size) {
      printf(" ");
      if ((i+1) % 16 == 0) {
        printf("|  %s \n", ascii);
      } else if (i+1 == size) {
        ascii[(i+1) % 16] = '\0';
        if ((i+1) % 16 <= 8) {
          printf(" ");
        }
        for (j = (i+1) % 16; j < 16; ++j) {
          printf("   ");
        }
        printf("|  %s \n", ascii);
      }
    }
  }
}

typedef enum mnemonic {
  invalidop,
  mov,
  add,
  push,
  leave,
  lea,
  cmp,
  jmp,
  jbe,
  jb,
  jge,
  jg,
  call,
  ret,
} mnemonic;

typedef enum loct {
  invalidloc,
  regt,
  memt,
  numt,
} loct;

typedef enum reg {
  invalidreg,
  rax,
  rbx,
  rcx,
  rdx,
  rdi,
  rsi,
  rsp,
  rbp,
  rip,
} reg;

typedef struct mem {
  reg reg;
  int offset;
} mem;

typedef struct loc {
  loct type;
  union {
    reg reg;
    mem mem;
    int num;
  } inner;
} loc;

typedef struct instruction {
  mnemonic mnemonic;
  loc src;
  loc dst;
} instruction;

mnemonic parse_mnemonic(const char const *token) {
  #define PARSEOP(mnemonic) do { if (strcmp(token, #mnemonic) == 0) return mnemonic; } while(0)

  PARSEOP(mov);
  PARSEOP(add);
  PARSEOP(push);
  PARSEOP(leave);
  PARSEOP(lea);
  PARSEOP(cmp);
  PARSEOP(jbe);
  PARSEOP(jb);
  PARSEOP(jge);
  PARSEOP(jg);
  PARSEOP(jmp);
  PARSEOP(call);
  PARSEOP(ret);
  return invalidop;
}

reg parse_reg(char const *token) {
#define PARSEREG(mnemonic) do { if (strncmp(token, #mnemonic, sizeof(#mnemonic) - 1) == 0) return mnemonic; } while(0)
  
  PARSEREG(rax);
  PARSEREG(rbx);
  PARSEREG(rcx);
  PARSEREG(rdx);
  PARSEREG(rdi);
  PARSEREG(rsi);
  PARSEREG(rsp);
  PARSEREG(rbp);
  PARSEREG(rip);
  return invalidreg;
}

loc parse_loc(char const *token) {
  loc inner;
  loc loc = {0};
  if (*token == '%') {
    loc.type = regt;
    ++token;
    loc.inner.reg = parse_reg(token);
  } else if (*token == '(') {
    loc.type = memt;
    loc.inner.mem.offset = 0;
    ++token;
    inner = parse_loc(token);
    loc.inner.mem.reg = inner.inner.mem.reg;
  } else {
    /* printf("%s\n", token); */
    char *end;
    int value = strtol(token, &end, 10);
    if (token == end)
      return loc;
    token = end;
    inner = parse_loc(token);
    if (inner.type == memt) {
      loc = inner;
      loc.inner.mem.offset = value;
    } else {
      loc.type = numt;
      loc.inner.num = value;
    }
  }
  return loc;
}

instruction parse_instruction(const char const *str) {
  char token[64] = {0};
  char *token_end;
  instruction i = {0};

  /* mnemonic */
  if(((token_end = strchr(str, ' ')) == NULL) && ((token_end = strchr(str, ';')) == NULL)) {
    return i;
  }
  strncpy(token, str, token_end - str);
  token[token_end - str] = '\0';
  i.mnemonic = parse_mnemonic(token);
  if (*token_end == ';') return i;

  /* src */
  str = token_end + 1;
  if(((token_end = strchr(str, ',')) == NULL) && ((token_end = strchr(str, ';')) == NULL)) {
    return i;
  }
  strncpy(token, str, token_end - str);
  token[token_end - str] = '\0';
  i.src = parse_loc(token);
  if (*token_end == ';') return i;

  /* dst */
  str = token_end + 1;
  if((token_end = strchr(str, ';')) == NULL) {
    return i;
  }
  strncpy(token, str, token_end - str);
  token[token_end - str] = '\0';
  i.dst = parse_loc(token);

  return i;
}

void print_loc(loc *loc) {
  if (loc->type == regt) {
    printf("(r) %d", loc->inner.reg);
  } else if (loc->type == memt) {
    printf("(m) %d(%d)", loc->inner.mem.offset, loc->inner.mem.reg);
  } else if (loc->type == numt) {
    printf("(n) %d", loc->inner.num);
  }
}

void print_instruction(instruction *i) {
  printf("%d   ", i->mnemonic);
  print_loc(&i->src);
  printf(" => ");
  print_loc(&i->dst);
  printf("\n");
}

// returns 3 bits
char assemble_reg(reg reg) {
  // TODO generalize size eax=rax
  switch (reg) {
  case rax: return 0x00;
  case rcx: return 0x01;
  case rdx: return 0x02;
  case rbx: return 0x03; // FIXME CHECK
  case rsp: return 0x04;
  case rbp: return 0x05;
  case rsi: return 0x06;
  case rdi: return 0x07;
  default:
    printf("[-] assemble_reg: invalid reg %d\n", reg);
  }
}

void assemble_instruction(char **mem, instruction i) {
  switch (i.mnemonic) {
  case push:
    if (i.src.type == regt) {
      char inst[] = {0x50 + assemble_reg(i.src.inner.reg)};
      
      strncpy(*mem, inst, 1);
      *mem += 1;
    } else {
      printf("unsupported operand for push\n");
    }
    return;
  case leave:
    char inst[] = {0xC9};
      
    strncpy(*mem, inst, 1);
    *mem += 1;
    return;
  case ret:
    inst[0] = 0xC3; // what is this
      
    strncpy(*mem, inst, 1);
    *mem += 1;
    return;
  case mov:
    //  401880:       48 89 e5                mov    %rsp,%rbp
    //                   1000 1001 11 100 101
    //  4018dd:       48 89 c7                mov    %rax,%rdi
    //                   1000 1001 11 000 111
    //  4018f1:       48 89 e0                mov    %rsp,%rax
    //                   1000 1001 11 100 000
    //  40191b:       48 89 d7                mov    %rdx,%rdi
    //                   1000 1001 11 010 111
    //  40138e:       0f b6 00                movzbl (%rax),%eax

    //  4017e2:       8b 45 10                mov    0x10(%rbp),%eax
    //                   0100 0101 00 010 000
    //  4018fc:       48 89 08                mov    %rcx,(%rax)
    //                   1000 1001 00 001 000
    //  4016fd:       48 89 58 08             mov    %rbx,0x8(%rax)
    //                   1000 1001 01 011 000  00001000
    //  4012dd:       48 89 45 b0             mov    %rax,-0x50(%rbp)
    //                   1000 1001 01 000 101  ...
    //
    //  4016e5:       48 8b 85 68 ff ff ff    mov    -0x98(%rbp),%rax
    //                   1000 1011 10 000 101  01101000 11111111 11111111 11111111
    //  4014c2:       48 c7 45 d8 00 00 00 00 movq   $0x0,-0x28(%rbp)
    //                   1011 0111 01 000 101  11011000 00000000 00000000 00000000 00000000

    if (i.src.type == regt && i.dst.type == regt) {
      char inst[] = {0x48, 0x89, 0xc0};
      inst[2] |= assemble_reg(i.src.inner.reg) << 3;
      inst[2] |= assemble_reg(i.dst.inner.reg) << 0;
      
      strncpy(*mem, inst, sizeof(inst)/sizeof(inst[0]));
      *mem += sizeof(inst)/sizeof(inst[0]);
    } else if (i.src.type == memt && i.dst.type == regt) {
      char inst[] = {0x48, 0x8b, 0x80, 0, 0, 0, 0};
      inst[2] |= assemble_reg(i.src.inner.mem.reg) << 0; // Why are these flipped?
      inst[2] |= assemble_reg(i.dst.inner.reg) << 3;
      *(int*)(&inst[3]) = i.src.inner.mem.offset;
      
      strncpy(*mem, inst, sizeof(inst)/sizeof(inst[0]));
      *mem += sizeof(inst)/sizeof(inst[0]);
    } else if (i.src.type == regt && i.dst.type == memt) {
      // FIXME negative offset doesn't work for "mov %rdi,-8(%rbp);"?
      char inst[] = {0x48, 0x89, 0x40, 0};
      inst[2] |= assemble_reg(i.src.inner.reg) << 3;
      inst[2] |= assemble_reg(i.dst.inner.mem.reg) << 0;
      inst[3] = (char) i.dst.inner.mem.offset;
      
      strncpy(*mem, inst, sizeof(inst)/sizeof(inst[0]));
      *mem += sizeof(inst)/sizeof(inst[0]);
    } else {
      printf("unsupported operand for mov\n");
    }
    return;
  case add:
    //  401923:       48 83 c4 30             add    $0x30,%rsp
    //                      11000 100
    //  4013a0:       48 83 45 d8 01          addq   $0x1,-0x28(%rbp)
    //  401302:       01 d0                   add    %edx,%eax
    //                0000 0001 1101 0000
    //  40119f:       48 01 d0                add    %rdx,%rax
    //  4012d3:       48 83 45 f8 01          addq   $0x1,-0x8(%rbp)
    //                      01000 101
    if (i.src.type == regt && i.dst.type == regt) {
      char inst[] = {0x48, 0x01, 0xc0};
      inst[2] |= assemble_reg(i.src.inner.reg) << 3;
      inst[2] |= assemble_reg(i.dst.inner.reg) << 0;
      
      strncpy(*mem, inst, sizeof(inst)/sizeof(inst[0]));
      *mem += sizeof(inst)/sizeof(inst[0]);
    } else if (i.src.type == numt && i.dst.type == regt) {
      char inst[] = {0x48, 0x83, 0xc0, 0};
      inst[2] |= assemble_reg(i.dst.inner.reg) << 0;
      // FIXME char
      inst[3] = (char) i.src.inner.num;
      
      strncpy(*mem, inst, sizeof(inst)/sizeof(inst[0]));
      *mem += sizeof(inst)/sizeof(inst[0]);
    } else if (i.src.type == numt && i.dst.type == memt) {
      char inst[] = {0x48, 0x83, 0x40, 0, 0};
      inst[2] |= assemble_reg(i.src.inner.reg) << 0;
      // FIXME char
      inst[3] = (char) i.dst.inner.mem.offset;
      inst[4] = (char) i.src.inner.num;
      
      strncpy(*mem, inst, sizeof(inst)/sizeof(inst[0]));
      *mem += sizeof(inst)/sizeof(inst[0]);
    } else {
      printf("unsupported operand for add\n");
    }
    return;
  case call:
    //  401c46:       e8 f5 f3 ff ff          call   401040 <strncpy@plt>
    if (i.src.type == memt && i.src.inner.mem.reg == rip) {
      char inst[] = {0xe8, 0, 0, 0, 0};
      *(int*)(&inst[1]) = i.src.inner.mem.offset;

      strncpy(*mem, inst, sizeof(inst)/sizeof(inst[0]));
      *mem += sizeof(inst)/sizeof(inst[0]);
    } else {
      printf("unsupported operand for call\n");
    }
    return;
  case jmp:
    //  401d8a:       eb 01                   jmp    401d8d <assemble_instruction+0x2de>
    //  401ca0:       e9 e1 00 00 00          jmp    401d86 <assemble_instruction+0x2d7>
    if (i.src.type == memt && i.src.inner.mem.reg == rip) {
      char inst[] = {0xe9, 0, 0, 0, 0};
      *(int*)(&inst[1]) = i.src.inner.mem.offset;

      strncpy(*mem, inst, sizeof(inst)/sizeof(inst[0]));
      *mem += sizeof(inst)/sizeof(inst[0]);
    } else {
      printf("unsupported operand for jmp\n");
    }
    return;
  case cmp:
    //  401bee:       83 f8 01                cmp    $0x1,%eax
    //  4012d3:       48 83 f8 00             cmp    $0x0,%rax
    //   23cb1:       48 81 f9 cf 0f 00 00    cmp    $0xfcf,%rcx
    //                   1000 0001 11 111 001
    //  4010f5:       48 3d 50 40 40 00       cmp    $0x404050,%rax
    //                   0111 1101   where is rax?
    //   1cb88:          81 f9 52 e5 74 64       cmp    $0x6474e552,%ecx
    //  40181c:       48 83 7d f8 00          cmpq   $0x0,-0x8(%rbp)
    //                   1000 0011 01 111 101
    //   23ec6:       39 c1                   cmp    %eax,%ecx
    //   257d4:       49 39 c2                cmp    %rax,%r10
    if (i.src.type == numt && i.dst.type == regt && i.dst.inner.reg == rax) {
      // TODO support comparing othe registers also
      char inst[] = {0x48, 0x83, 0xf8, 0};
      inst[2] |= assemble_reg(i.dst.inner.reg) << 0;
      *(int*)(&inst[3]) = i.src.inner.num;

      strncpy(*mem, inst, sizeof(inst)/sizeof(inst[0]));
      *mem += sizeof(inst)/sizeof(inst[0]);
    } else if (i.src.type == numt && i.dst.type == memt) {
      char inst[] = {0x48, 0x83, 0x78, 0 /* offset */, 0 /* op1 */};
      inst[2] |= assemble_reg(i.dst.inner.mem.reg) << 0;
      // FIXME char
      inst[3] = (char) i.dst.inner.mem.offset;
      inst[4] = (char) i.src.inner.num;

      strncpy(*mem, inst, sizeof(inst)/sizeof(inst[0]));
      *mem += sizeof(inst)/sizeof(inst[0]);
    } else {
      printf("unsupported operand for cmp\n");
    }
    return;
  case jbe:
  case jb:
  case jge:
  case jg:
    //   24e06:       0f 86 68 08 00 00       jbe    25674 <strncmp+0x1844>
    //   253c4:       0f 8f a6 00 00 00       jg     25470 <strncmp+0x1640>
    char opcode = 0x86;
    if (i.mnemonic == jb) opcode = 0x82;
    if (i.mnemonic == jge) opcode = 0x8d;
    if (i.mnemonic == jg) opcode = 0x8f;
    if (i.src.type == memt && i.src.inner.mem.reg == rip) {
      char inst[] = {0x0f, opcode, 0, 0, 0, 0};
      *(int*)(&inst[2]) = i.src.inner.mem.offset;

      strncpy(*mem, inst, sizeof(inst)/sizeof(inst[0]));
      *mem += sizeof(inst)/sizeof(inst[0]);
    } else {
      printf("unsupported operand for jbe\n");
    }
    return;
  }
  printf("invalid instruction!\n");
}

int main() {
  char *origmem = mmap(0, BODY_SIZE, PROT_READ|PROT_WRITE|PROT_EXEC, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0);
  char *mem = origmem;

  char *func[] = {
    "push %rbp;",
    "mov %rsp,%rbp;",
    "mov %rdi,%rax;",
    "cmp 2,%rax;",        // if n < 2
    "jbe 47(%rip);",      //   return n
    "add -16,%rsp;",
    "mov %rdi,-8(%rbp);", // -8 = n
    "add -1,%rdi;",
    "call -34(%rip);",
    "mov %rax,-16(%rbp);",// -16 = fib(n-1)
    "mov -8(%rbp),%rdi;",
    "add -2,%rdi;",
    "call -54(%rip);",    // fib(n-2)
    "mov -16(%rbp),%rbx;",
    "add %rbx,%rax;",     // +
    "leave;",
    "ret;",
  };
  
  for (size_t c = 0; c < sizeof(func)/sizeof(func[0]); ++c) {
    instruction i = parse_instruction(func[c]);
    print_instruction(&i);
    assemble_instruction(&mem, i);
  }
  
  dumphex(origmem, BODY_SIZE);

  int (*fib)(int) = (void*) origmem;

  int out = fib(10);

  printf("fib(10) = %d\n", out);
  
  return 0;
}

int sum(int a, int b) {
  return a + b;
}
